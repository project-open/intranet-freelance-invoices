-- upgrade-5.0.2.4.0-5.0.2.4.1.sql
SELECT acs_log__debug('/packages/intranet-freelance-invoices/sql/postgresql/upgrade/upgrade-5.0.2.4.0-5.0.2.4.1.sql','');


create or replace function inline_0 () 
returns integer as $body$
DECLARE
	v_count			integer;
BEGIN
	-- Check if colum exists in the database
	select	count(*) into v_count from user_tab_columns 
	where	lower(table_name) = 'wf_roles' and
		lower(column_name) = 'freelance_task_type_id';
	IF v_count > 0  THEN return 1; END IF; 

	alter table wf_roles add freelance_task_type_id integer references im_categories;

	return 0;
END;$body$ language 'plpgsql';
SELECT inline_0 ();
DROP FUNCTION inline_0 ();



-- Function to map workflow role (=transition) to translation task
--
create or replace function im_trans_freelance_project_type_from_wf_role (integer, varchar) 
returns integer as $body$
DECLARE
	p_case_id		alias for $1;
	p_role_key		alias for $2;

	v_project_type_id	integer;
BEGIN

	select	freelance_task_type_id into v_project_type_id
	from	wf_roles
	where	workflow_key in (select workflow_key from wf_cases where case_id = p_case_id) and
		role_key = p_role_key;
	IF v_project_type_id is not null THEN
		RAISE NOTICE 'im_trans_freelance_project_type_from_wf_role(%,%): Found freelance_task_type_id in wf_roles: %', p_case_id, p_role_key, v_project_type_id;
		return v_project_type_id; 
	END IF;


	-- Check if there is a project type with the same name as the transition
	select	min(category_id) into v_project_type_id
	from	im_categories
	where	category_type = 'Intranet Project Type' and
		lower(trim(category)) = lower(trim(p_role_key));
	IF v_project_type_id is not null THEN
		RAISE NOTICE 'im_trans_freelance_project_type_from_wf_role(%,%): Found matching project type: %', p_case_id, p_role_key, v_project_type_id;	     
		return v_project_type_id; 
	END IF;


	return 86;	-- other

END;$body$ language 'plpgsql';
